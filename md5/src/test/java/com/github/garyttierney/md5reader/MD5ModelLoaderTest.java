package com.github.garyttierney.md5reader;

import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MD5ModelLoaderTest {

    @Test
    public void testLoad() throws Exception {
        try (InputStream is = MD5ModelLoaderTest.class.getResourceAsStream("/test.md5mesh")) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
                MD5ModelLoader loader = new MD5ModelLoader(reader, null);
                MD5Model model = loader.load();

                Assert.assertEquals(1, model.getNumMeshes());
                Assert.assertEquals(1, model.getNumJoints());

                MD5Mesh mesh = model.getMesh(0);

                Assert.assertEquals(3, mesh.getNumVertices());
                for (int i = 0; i < mesh.getNumVertices(); i++) {
                    MD5Mesh.Vertex vertex = mesh.getVertex(i);


                }
                Assert.assertEquals(1, mesh.getNumTriangles());
            }
        }
    }
}