package com.github.garyttierney.md5reader;

import org.junit.Assert;
import org.junit.Test;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

public class MD5ParserUtilsTest {

    @Test
    public void testParseJoint() throws Exception {
        String input = "\t\"origin\"\t-1 ( -0.000000 0.016430 -0.006044 ) ( 0.707107 0.000000 0.707107 )\t\t//";

        MD5Joint joint = MD5ParserUtils.parseJoint(input);

        Assert.assertEquals("origin", joint.getName());
        Assert.assertEquals(-1, joint.getParentID());
        Assert.assertEquals(new Vector3f(-0.0f, 0.016430f, -0.006044f), joint.getPosition());
        Assert.assertEquals(new Quat4f(0.707107f, 0.0f, 0.707107f, 0.0f), joint.getOrientation());
    }

    @Test
    public void testParseVertex() throws Exception {
        String input="vert 8 ( 0.537109 0.433594 ) 13 3";

        MD5Mesh.Vertex vertex = MD5ParserUtils.parseVertex(input);

        Assert.assertEquals(0.537109f, vertex.u, 0.0f);
        Assert.assertEquals(0.433594f, vertex.v, 0.0f);
        Assert.assertEquals(13, vertex.startWeight);
        Assert.assertEquals(3, vertex.weightCount);
    }

    @Test
    public void testParseTriangle() throws Exception {
        String input="tri 0 0 1 2";

        MD5Mesh.Triangle tri = MD5ParserUtils.parseTriangle(input);

        Assert.assertEquals(0, tri.aVertexIndex);
        Assert.assertEquals(1, tri.bVertexIndex);
        Assert.assertEquals(2, tri.cVertexIndex);
    }
}