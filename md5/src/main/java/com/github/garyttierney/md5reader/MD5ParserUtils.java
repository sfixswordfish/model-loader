package com.github.garyttierney.md5reader;


import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MD5ParserUtils {
    private static final String FP_REGEXP = "([\\-\\+]?[0-9]*\\.[0-9]+)";
    private static final String VEC3F_REGEXP = String.format("%1$s %1$s %1$s", FP_REGEXP);
    private static final String WEIGHT_REGEXP = String.format("[\\s\t]*weight (\\d+) (\\d+) %s \\( %s \\)", FP_REGEXP, VEC3F_REGEXP);
    private static final String VERTEX_REGEXP = String.format("[\\s\t]*vert \\d+ \\( %1$s %1$s \\) (\\d+) (\\d+)", FP_REGEXP);
    private static final String TRIANGLE_REGEXP = "[\\s\t]*tri \\d+ (\\d+) (\\d+) (\\d+)";
    private static final Pattern JOINT_PATTERN = Pattern.compile("\"([a-z][A-Z]+)\" ([\\-]?\\d+) \\( " + VEC3F_REGEXP + "\\) \\( " + VEC3F_REGEXP + "\\)");

    private static final Pattern WEIGHT_PATTERN = Pattern.compile(WEIGHT_REGEXP);
    private static final Pattern VERTEX_PATTERN = Pattern.compile(VERTEX_REGEXP);

    /**
     * Parse a MD5Joint instance from the given input.
     *
     * @param input
     *
     * @return A parsed MD5Joint object.
     * @todo - rewrite to use regexp
     */
    public static MD5Joint parseJoint(String input) {
        String sanitizedInput = input
            .trim()
            .replaceAll("\"", "")
            .replaceAll("\\(|\\)", "");

        Scanner scanner = new Scanner(sanitizedInput);

        String name = scanner.next();

        int parentID = scanner.nextInt();

        float posX = scanner.nextFloat();
        float posY = scanner.nextFloat();
        float posZ = scanner.nextFloat();

        float orientX = scanner.nextFloat();
        float orientY = scanner.nextFloat();
        float orientZ = scanner.nextFloat();

        float t = 1.0f - (orientX * orientX) - (orientY * orientY) - (orientZ * orientZ);
        float w;

        if (t < 0.0f) {
            w = 0.0f;
        } else {
            w = (float) -StrictMath.sqrt(t);
        }

        return new MD5Joint(name, parentID, new Vector3f(posX, posY, posZ), new Quat4f(orientX, orientY, orientZ, w));
    }

    /**
     * Parse and return a mesh in the format of
     *
     * <pre>
     * mesh {
     *     shader "<string>"
     *
     *     numverts <int>
     *     vert vertIndex ( u v ) startWeight countWeight
     *     vert ...
     *
     *     numtris <int>
     *     tri triIndex vertIndex[0] vertIndex[1] vertIndex[2]
     *     tri ...
     *
     *     numweights <int>
     *     weight <i>index</i> <i>joint</i> <i>bias</i> <i>( pos.x pos.y pos.z )</i>
     *     weight ...
     * }
     * </pre>
     *
     * @param reader The reader to parse the mesh from.
     *
     * @return A parsed MD5Mesh object.
     *
     * @throws IOException If an error occurred while parsing data from the reader.
     */
    public static MD5Mesh parseMesh(BufferedReader reader) throws IOException {
        String input;

        MD5Mesh mesh = new MD5Mesh();

        while ((input = reader.readLine().trim()) != null && !input.contains("}")) {
            String[] parts = input.split(" ");

            if (input.startsWith("shader")) {
                mesh.setShaderName(parts[1].replaceAll("\"", ""));
            } else if (input.startsWith("numverts")) {
                mesh.setNumVertices(Integer.parseInt(parts[1]));
                for (int vertIndex = 0; vertIndex < mesh.getNumVertices(); vertIndex++) {
                    mesh.setVertex(vertIndex, parseVertex(reader.readLine()));
                }
            } else if (input.startsWith("numtris")) {
                mesh.setNumTriangles(Integer.parseInt(parts[1]));
                for (int triIndex = 0; triIndex < mesh.getNumTriangles(); triIndex++) {
                    mesh.setTriangle(triIndex, parseTriangle(reader.readLine()));
                }
            } else if (input.startsWith("numweights")) {
                mesh.setNumWeights(Integer.parseInt(parts[1]));
                for (int weightIndex = 0; weightIndex < mesh.getNumWeights(); weightIndex++) {
                    mesh.setWeight(weightIndex, parseWeight(reader.readLine()));
                }
            }
        }

        return mesh;
    }

    public static MD5Mesh.Weight parseWeight(String input) {
        Matcher matcher = WEIGHT_PATTERN.matcher(input);
         if (!matcher.matches()) {
            throw new IllegalArgumentException("Given input is not a weight definition");
        }

        int id = Integer.parseInt(matcher.group(1));
        int joint = Integer.parseInt(matcher.group(2));
        float bias = Float.parseFloat(matcher.group(3));

        Vector3f position = new Vector3f(
            Float.parseFloat(matcher.group(4)),
            Float.parseFloat(matcher.group(5)),
            Float.parseFloat(matcher.group(6))
        );

        return new MD5Mesh.Weight(joint, bias, position);
    }

    /**
     * Parse a vertex from a given vertex definition, in the format of:
     * <p>
     * <pre>
     *     vert &lt;index&gt; ( &lt;u-coordinate&gt; &lt;v-coordinate&gt; ) &lt;weight-offset&gt; &lt;weight-count&gt;
     * </pre>
     *
     * @param input The input to parse.
     *
     * @return A Vertex instance.
     */
    public static MD5Mesh.Vertex parseVertex(String input) {
        Matcher matcher = VERTEX_PATTERN.matcher(input);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Given input is not a vertex definition");
        }

        float u = Float.parseFloat(matcher.group(1));
        float v = Float.parseFloat(matcher.group(2));

        int startWeight = Integer.parseInt(matcher.group(3));
        int weightsCount = Integer.parseInt(matcher.group(4));

        return new MD5Mesh.Vertex(u, v, startWeight, weightsCount);
    }

    /**
     * Parse a triangle object from the given triangle definition, in the format of:
     * <p>
     * <pre>
     *     tri &lt;index&gt; &lt;tri-a-index&gt; &lt;tri-b-index&gt; &lt;tri-c-index&gt;
     * </pre>
     *
     * @param input The triangle input.
     *
     * @return The parsed value object.
     */
    public static MD5Mesh.Triangle parseTriangle(String input) {
        Matcher matcher = Pattern.compile(TRIANGLE_REGEXP).matcher(input);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Given input is not a vertex definition");
        }

        int aIndex = Integer.parseInt(matcher.group(1));
        int bIndex = Integer.parseInt(matcher.group(2));
        int cIndex = Integer.parseInt(matcher.group(3));

        return new MD5Mesh.Triangle(aIndex, bIndex, cIndex);
    }
}
