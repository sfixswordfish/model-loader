package com.github.garyttierney.md5reader.io;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class DefaultModelResourceResolver implements ModelResourceResolver {
    private final Path basePath;

    public DefaultModelResourceResolver(Path basePath) {
        this.basePath = basePath;
    }

    @Override
    public List<Path> resolveAnimations() throws ModelResourceResolverException {
        try {
            DirectoryStream<Path> pathStream = Files.newDirectoryStream(basePath, "*.md5anim");

            return StreamSupport.stream(pathStream.spliterator(), false)
                .collect(Collectors.toCollection(ArrayList::new));
        } catch (IOException e) {
            throw new ModelResourceResolverException("Unable to run filter on *.md5anim glob", e);
        }
    }
}
