package com.github.garyttierney.md5reader.io;

import java.nio.file.Path;
import java.util.List;

public interface ModelResourceResolver {
    public List<Path> resolveAnimations() throws ModelResourceResolverException;

}
