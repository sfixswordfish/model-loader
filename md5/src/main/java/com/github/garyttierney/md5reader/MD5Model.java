package com.github.garyttierney.md5reader;

import javax.vecmath.Matrix4f;

public class MD5Model {
    private Matrix4f localToWorld;

    private int numMeshes;
    private int numJoints;
    private MD5Joint[] joints;
    private MD5Mesh[] meshes;

    public int getNumMeshes() {
        return numMeshes;
    }

    public void setNumMeshes(int numMeshes) {
        this.numMeshes = numMeshes;
        this.meshes = new MD5Mesh[numMeshes];
    }

    public int getNumJoints() {
        return numJoints;
    }

    public void setNumJoints(int numJoints) {
        this.numJoints = numJoints;
        this.joints = new MD5Joint[numJoints];
    }

    public void setJoint(int index, MD5Joint joint) {
        this.joints[index] = joint;
    }

    public void setMesh(int index, MD5Mesh mesh) {
        this.meshes[index] = mesh;
    }

    public MD5Mesh getMesh(int index) {
        return meshes[index];
    }
}
