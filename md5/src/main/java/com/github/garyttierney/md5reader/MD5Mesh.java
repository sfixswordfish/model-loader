package com.github.garyttierney.md5reader;

import javax.vecmath.Vector3f;

/**
 * struct Mesh { std::string     m_Shader; // This vertex list stores the vertices in the bind pose. VertexList      m_Verts; TriangleList
 * m_Tris; WeightList      m_Weights;
 * <p>
 * // A texture ID for the material GLuint          m_TexID;
 * <p>
 * // These buffers are used for rendering the animated mesh PositionBuffer  m_PositionBuffer;   // Vertex position stream NormalBuffer
 * m_NormalBuffer;     // Vertex normals stream Tex2DBuffer     m_Tex2DBuffer;      // Texture coordinate set IndexBuffer     m_IndexBuffer;
 * // Vertex index buffer };
 */
public class MD5Mesh {
    /**
     * Name of the shader resource associated with this mesh.
     */
    private String shaderName;

    /**
     * An array of vectors which make up the vertices of this mesh.
     */
    private Vertex[] vertices;

    /**
     * An array of {@link Triangle}s which map triangle points to vertex indexes.
     */
    private Triangle[] triangles;

    /**
     * An array of weights which make up the vertex weights of this mesh.
     */
    private Weight[] weights;

    /**
     * Get the number of vertices in this mesh.
     *
     * @return The number of vertices in this mesh.
     */
    public int getNumVertices() {
        return vertices.length;
    }

    /**
     * Set the number of vertices and re-initialize the vertices array.
     *
     * @param numVertices The new number of vertices.
     */
    public void setNumVertices(int numVertices) {
        this.vertices = new Vertex[numVertices];
    }

    /**
     * Get the number of triangles in this mesh.
     *
     * @return The number of triangles in this mesh.
     */
    public int getNumTriangles() {
        return triangles.length;
    }

    /**
     * Set the number of triangles and re-initialize the triangles array.
     *
     * @param numTriangles The new number of triangles.
     */
    public void setNumTriangles(int numTriangles) {
        this.triangles = new Triangle[numTriangles];
    }

    /**
     * Set the vertex at the given index.
     *
     * @param index  The index of the vertex.
     * @param vertex The vertex value.
     */
    public void setVertex(int index, Vertex vertex) {
        this.vertices[index] = vertex;
    }

    public Vertex getVertex(int index) {
        return vertices[index];
    }

    /**
     * Set the triangle at the given index.
     *
     * @param index    The index of the triangle.
     * @param triangle The triangle value.
     */
    public void setTriangle(int index, Triangle triangle) {
        this.triangles[index] = triangle;
    }

    public String getShaderName() {
        return shaderName;
    }

    public void setShaderName(String shaderName) {
        this.shaderName = shaderName;
    }

    public int getNumWeights() {
        return weights.length;
    }

    public void setNumWeights(int numWeights) {
        this.weights = new Weight[numWeights];
    }

    public void setWeight(int index, Weight weight) {
        this.weights[index] = weight;
    }

    /**
     * A value object which represents an MD5 vertex.
     */
    public static class Vertex {
        public final float u;
        public final float v;
        public final int startWeight;
        public final int weightCount;

        public Vertex(float u, float v, int startWeight, int weightCount) {
            this.u = u;
            this.v = v;
            this.startWeight = startWeight;
            this.weightCount = weightCount;
        }
    }

    /**
     * A value object which represents an MD5 format triangle.
     */
    public static class Triangle {
        public int aVertexIndex;
        public int bVertexIndex;
        public int cVertexIndex;

        public Triangle(int aVertexIndex, int bVertexIndex, int cVertexIndex) {
            this.aVertexIndex = aVertexIndex;
            this.bVertexIndex = bVertexIndex;
            this.cVertexIndex = cVertexIndex;
        }
    }

    /**
     * A value object which represents an MD5 format vertex weight.
     */
    public static class Weight {
        private final int joint;
        private final float bias;
        private final Vector3f position;

        public Weight(int joint, float bias, Vector3f position) {
            this.joint = joint;
            this.bias = bias;
            this.position = position;
        }
    }
}
