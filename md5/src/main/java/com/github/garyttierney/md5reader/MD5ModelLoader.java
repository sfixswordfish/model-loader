package com.github.garyttierney.md5reader;

import com.github.garyttierney.md5reader.io.DefaultModelResourceResolver;
import com.github.garyttierney.md5reader.io.ModelResourceResolver;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * A loader for the MD5 version 10 model format.
 *
 * @author Gary Tierney
 */
public class MD5ModelLoader {

    /**
     * The resource resolver implementation to be called when loading
     */
    private final ModelResourceResolver resourceResolver;
    /**
     * The input source we're reading the md5mesh from.
     */
    private final BufferedReader reader;
    /**
     * The model which is being loaded.
     */
    private final MD5Model model = new MD5Model();
    /**
     * The index of the last mesh to be loaded.
     */
    private int meshIndex = 0;
    /**
     * The version of model being loaded.
     */
    private int version = -1;

    /**
     * Create a new MD5ModelLoader with the md5mesh file at <code>path</code>. Use the {@link DefaultModelResourceResolver} to resolve
     * animations and textures from the same path the model file was loaded from.
     *
     * @param path The path to the animation.
     *
     * @throws IOException If a reader for <code>path</code> couldn't be created
     */
    public MD5ModelLoader(Path path) throws IOException {
        this(Files.newBufferedReader(path), new DefaultModelResourceResolver(path.getParent()));
    }

    /**
     * Create a new loader which reads the model from the given <code>reader</code> and resolves aniamtions / textures from
     * <code>resourceResolver</code>
     *
     * @param reader           The reader to read the model from.
     * @param resourceResolver The resolver to get animations and textures from.
     */
    public MD5ModelLoader(BufferedReader reader, ModelResourceResolver resourceResolver) {
        this.reader = reader;
        this.resourceResolver = resourceResolver;
    }

    /**
     * Load all the data into the given model and return it.
     *
     * @return The loaded model.
     * @throws MD5ModelLoaderException
     */
    public MD5Model load() throws MD5ModelLoaderException {
        String input;
        try {
            while ((input = reader.readLine()) != null) {
                handleInput(reader, input);
            }
        } catch (IOException e) {
            throw new MD5ModelLoaderException("Unable to load MD5 model file", e);
        }

        if (version != 10) {
            throw new MD5ModelLoaderException("Can only load MD5 version 10 files");
        }

        return model;
    }

    private void handleInput(BufferedReader reader, String input) throws IOException {
        String[] parts = input.trim().split(" ");

        if (input.startsWith("MD5Version")) {
            version = Integer.parseInt(parts[1]);
        } else if (input.startsWith("numMeshes")) {
            model.setNumMeshes(Integer.parseInt(parts[1]));
        } else if (input.startsWith("numJoints")) {
            model.setNumJoints(Integer.parseInt(parts[1]));
        } else if (input.startsWith("joints")) {
            for (int i = 0; i < model.getNumJoints(); i++) {
                model.setJoint(i, MD5ParserUtils.parseJoint(reader.readLine()));
            }
        } else if (input.startsWith("mesh")) {
            model.setMesh(meshIndex++, MD5ParserUtils.parseMesh(reader));
        }
    }

}
