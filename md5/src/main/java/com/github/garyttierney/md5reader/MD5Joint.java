package com.github.garyttierney.md5reader;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

/**
 * struct Joint { std::string     m_Name; int             m_ParentID; glm::vec3       m_Pos; glm::quat       m_Orient };
 */
public class MD5Joint {

    private final String name;
    private final int parentID;
    private final Vector3f position;
    private final Quat4f orientation;

    public MD5Joint(String name, int parentID, Vector3f position, Quat4f orientation) {
        this.name = name;
        this.parentID = parentID;
        this.position = position;
        this.orientation = orientation;
    }

    public String getName() {
        return name;
    }

    public int getParentID() {
        return parentID;
    }

    public Vector3f getPosition() {
        return position;
    }

    public Quat4f getOrientation() {
        return orientation;
    }
}
