package com.github.garyttierney.models.core.animation;

public class SkeletonAnimationFrame {
    private final BoneState[] boneStates;

    public SkeletonAnimationFrame(BoneState[] boneStates) {
        this.boneStates = boneStates;
    }

    public boolean hasBoneState(int boneId) {
        return boneStates[boneId] != null;
    }

    public BoneState getBoneState(int boneId) {
        return boneStates[boneId];
    }
}
