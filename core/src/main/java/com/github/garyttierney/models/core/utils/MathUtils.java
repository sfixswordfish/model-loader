package com.github.garyttierney.models.core.utils;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

public class MathUtils {
    public static void rotate(Quat4f orient, Vector3f point) {
        Quat4f inverse = new Quat4f(orient);
        inverse.inverse();
        inverse.normalize();

        Quat4f tmp = new Quat4f(orient);
        mulQuat(tmp, point);
        tmp.mul(inverse);

        point.x = tmp.x;
        point.y = tmp.y;
        point.z = tmp.z;
    }

    public static void mulQuat(Quat4f src, Vector3f vec) {
        src.w = -(src.w * vec.x) - (src.y * vec.y) - (src.z * vec.z);
        src.x =  (src.w * vec.x) + (src.y * vec.z) - (src.z * vec.y);
        src.y =  (src.w * vec.y) + (src.z * vec.x) - (src.x * vec.z);
        src.z =  (src.w * vec.z) + (src.x * vec.y) - (src.y * vec.x);
    }
}
