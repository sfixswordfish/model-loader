package com.github.garyttierney.models.core.animation;

import javax.vecmath.Vector3f;

public class BoneWeight {
    public int boneId;
    public float bias;
    public Vector3f position;
}
