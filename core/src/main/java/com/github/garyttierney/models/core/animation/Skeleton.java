package com.github.garyttierney.models.core.animation;

public class Skeleton {
    private Bone[] bones;
    private BoneWeight[] boneWeights;
    private int[] vertexWeightOffsets;
    private int[] vertexWeightCounts;

    public Skeleton(int numBones, int numWeights, int numVertices) {
        this.bones = new Bone[numBones];
        this.boneWeights = new BoneWeight[numWeights];
        this.vertexWeightCounts = new int[numVertices];
        this.vertexWeightOffsets = new int[numVertices];
    }

    public int getNumberOfBones() {
        return bones.length;
    }

    public Bone getBone(int boneId) {
        return bones[boneId];
    }

    public BoneWeight getBoneWeight(int weightId) {
        return boneWeights[weightId];
    }

    public int getVertexWeightCount(int vert) {
        return vertexWeightCounts[vert];
    }

    public int getVertexWeightOffset(int vert) {
        return vertexWeightOffsets[vert];
    }
}
