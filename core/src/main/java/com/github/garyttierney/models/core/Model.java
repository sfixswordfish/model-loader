package com.github.garyttierney.models.core;

import javax.vecmath.Vector3f;

public class Model {
    private Vector3f vertices[];

    public int getVertexCount() {
        return vertices.length;
    }

    public Vector3f getVertex(int offset) {
        return vertices[offset];
    }
}
