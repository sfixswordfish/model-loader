package com.github.garyttierney.models.core.animation;

import com.github.garyttierney.models.core.Model;
import com.github.garyttierney.models.core.utils.MathUtils;

import javax.vecmath.Vector3f;

public class SkeletonAnimation implements Animation {

    private final Skeleton skeleton;
    private final SkeletonAnimationFrame[] frames;
    private final float frameRate;
    private final float maxTime;

    private float lastTime = 0f;
    private int currentFrameOffset = 0;
    private int nextFrameOffset = 1;

    public SkeletonAnimation(Skeleton skeleton, SkeletonAnimationFrame[] frames, float frameRate) {
        this.skeleton = skeleton;
        this.frames = frames;
        this.frameRate = frameRate;
        this.maxTime = 1.0f / frameRate;
    }

    @Override
    public void update(float deltaTime) {
        int maxFrame = frames.length - 1;

        lastTime += deltaTime;

        if (lastTime >= maxTime) {
            currentFrameOffset++;
            nextFrameOffset++;

            lastTime = 0f;

            if (currentFrameOffset > maxFrame) {
                currentFrameOffset = 0;
            }

            if (nextFrameOffset > maxFrame) {
                nextFrameOffset = 0;
            }
        }

        float t = lastTime * frameRate;

        for(int boneId = 0; boneId < skeleton.getNumberOfBones(); boneId++) {
            Bone bone = skeleton.getBone(boneId);

            SkeletonAnimationFrame currentFrame = frames[currentFrameOffset];
            SkeletonAnimationFrame nextFrame = frames[nextFrameOffset];

            if (currentFrame.hasBoneState(boneId) && nextFrame.hasBoneState(boneId)) {
                BoneState currentState = currentFrame.getBoneState(boneId);
                BoneState nextState = nextFrame.getBoneState(boneId);

                bone.state.position = currentState.position;
                bone.state.orientation = currentState.orientation;
                bone.state.position.interpolate(nextState.position, t);
                bone.state.orientation.interpolate(nextState.orientation, t);
            }
        }
    }

    @Override
    public void apply(Model model) {

        for (int vertId = 0; vertId < model.getVertexCount(); vertId++) {
            Vector3f vec = model.getVertex(0);
            vec.x = 0; vec.y = 0; vec.z = 0;

            for(int weightId = 0; weightId < skeleton.getVertexWeightCount(vertId); weightId++) {
                BoneWeight weight = skeleton.getBoneWeight(weightId);
                Bone bone = skeleton.getBone(weight.boneId);

                Vector3f weightedPos = new Vector3f(weight.position);
                MathUtils.rotate(bone.state.orientation, weightedPos);

                weightedPos.add(bone.state.position);
                weightedPos.scale(weight.bias);

                vec.add(weightedPos);
            }
        }
    }
}
