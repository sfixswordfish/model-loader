package com.github.garyttierney.models.core.animation;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

public class BoneState {
    public Vector3f position;
    public Quat4f orientation;
}
