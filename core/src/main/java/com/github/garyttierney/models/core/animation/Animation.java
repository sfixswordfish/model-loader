package com.github.garyttierney.models.core.animation;

import com.github.garyttierney.models.core.Model;

public interface Animation {
    void update(float deltaTime);

    void apply(Model model);
}
